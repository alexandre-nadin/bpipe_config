#!/bin/bash

function man() {
  echo -e "\nUsage: $(basename $0) [deploy-directory]"
}

TOOL_DIR="bpipeconfig"
CURRENT=`pwd -P`
[ "$1" ] && DEPLOY_DIR="$1" || DEPLOY_DIR=$(dirname $CURRENT)
[ ! -d "$DEPLOY_DIR" ] && echo "Error, $DEPLOY_DIR is not a directory." && man && exit

gradle clean && gradle dist
cd "$DEPLOY_DIR/"
rm -f "$TOOL_DIR"_SNAPSHOT.tar.gz
tar czvf "$TOOL_DIR"_SNAPSHOT.tar.gz $TOOL_DIR/
cd ${CURRENT}/build
rm -rf "$DEPLOY_DIR/$TOOL_DIR"
tar xzvf "$TOOL_DIR"-*.tar.gz -C "$DEPLOY_DIR/"
chmod -R g-w "$DEPLOY_DIR"
cd ..
